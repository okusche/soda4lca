# FROM maven:3.8.2-jdk-11
FROM markhobson/maven-chrome:jdk-11

LABEL \
	Maintainer = "ok*worx <info@okworx.com>" \
	MailingList = "soda4LCA@lists.kit.edu" \
	License = "GNU Affero General Public License (AGPL) 3.0" \
    Purpose = "Caches all maven dependencies"

# gpg: key 3A79BD29: public key "MySQL Release Engineering <mysql-build@oss.oracle.com>" imported
RUN set -ex; \
	key='bca43417c3b485dd128ec6d4b7b3b788a8d3785c'; \
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --version; \
	gpg -v --batch --keyserver keyserver.ubuntu.com --recv-keys "$key"; \
	gpg -v --batch --output /etc/apt/trusted.gpg.d/mysql.gpg --export "$key"; \
	gpgconf --kill all; \
	rm -rf "$GNUPGHOME"; \
	apt-key list > /dev/null

RUN wget http://security.ubuntu.com/ubuntu/pool/universe/n/ncurses/libtinfo5_6.3-2ubuntu0.1_amd64.deb; \
    apt install ./libtinfo5_6.3-2ubuntu0.1_amd64.deb; \
    wget http://launchpadlibrarian.net/646633572/libaio1_0.3.113-4_amd64.deb; \
    apt install ./libaio1_0.3.113-4_amd64.deb

ENV MYSQL_MAJOR 5.7
ENV MYSQL_VERSION 5.7.\*

RUN echo "deb http://repo.mysql.com/apt/ubuntu/ bionic mysql-5.7" > /etc/apt/sources.list.d/mysql.list

RUN apt-get update; \
	apt-get install -y mysql-client=${MYSQL_VERSION} gettext git git-lfs

# the "/var/lib/mysql" stuff here is because the mysql-server postinst doesn't have an explicit way to disable the mysql_install_db codepath besides having a database already "configured" (ie, stuff in /var/lib/mysql/mysql)
# also, we set debconf keys to make APT a little quieter
RUN { \
		echo mysql-community-server mysql-community-server/data-dir select ''; \
		echo mysql-community-server mysql-community-server/root-pass password ''; \
		echo mysql-community-server mysql-community-server/re-root-pass password ''; \
		echo mysql-community-server mysql-community-server/remove-test-db select false; \
	} | debconf-set-selections \
	&& apt-get install -y mysql-community-server="${MYSQL_VERSION}" && rm -rf /var/lib/apt/lists/* \
	&& rm -rf /var/lib/mysql && mkdir -p /var/lib/mysql /var/run/mysqld \
	&& chown -R mysql:mysql /var/lib/mysql /var/run/mysqld \
# ensure that /var/run/mysqld (used for socket and lock files) is writable regardless of the UID our mysqld instance ends up having at runtime
	&& chmod 1777 /var/run/mysqld /var/lib/mysql \
# comment out a few problematic configuration values
	&& find /etc/mysql/ -name '*.cnf' -print0 \
		| xargs -0 grep -lZE '^(bind-address|log)' \
		| xargs -rt -0 sed -Ei 's/^(bind-address|log)/#&/' \
# don't reverse lookup hostnames, they are usually another container
	&& echo '[mysqld]\nskip-host-cache\nskip-name-resolve' > /etc/mysql/conf.d/docker.cnf


# COPY --from=markhobson/maven-chrome /opt/google/chrome/google-chrome /opt/google/chrome/google-chrome
# COPY --from=markhobson/maven-chrome /opt/chromedriver* /usr/bin/chromedriver

# ARG CHROME_VERSION=96.0.4664.45-1
# RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
# 	&& echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
# 	&& apt-get update -qqy \
# 	&& apt-get -qqy install google-chrome-stable=$CHROME_VERSION \
# 	&& rm /etc/apt/sources.list.d/google-chrome.list \
# 	&& rm -rf /var/lib/apt/lists/* /var/cache/apt/* \
# 	&& sed -i 's/"$HERE\/chrome"/"$HERE\/chrome" --no-sandbox/g' /opt/google/chrome/google-chrome

# # ChromeDriver

# ARG CHROME_DRIVER_VERSION=96.0.4664.45
# RUN wget -q -O /tmp/chromedriver.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip \
# 	&& unzip /tmp/chromedriver.zip -d /opt \
# 	&& rm /tmp/chromedriver.zip \
# 	&& mv /opt/chromedriver /opt/chromedriver-$CHROME_DRIVER_VERSION \
# 	&& chmod 755 /opt/chromedriver-$CHROME_DRIVER_VERSION \
# 	&& ln -s /opt/chromedriver-$CHROME_DRIVER_VERSION /usr/bin/chromedriver

RUN mysqld --initialize-insecure

COPY . soda4lca
RUN cd soda4lca \
	&& mvn --no-transfer-progress org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=$(find Node/lib/primefaces/ -name '*.jar') \
	&& mvn --no-transfer-progress org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=$(find Node/lib/shiro-faces/ -name '*.jar') \
	&& mvn --no-transfer-progress install -DskipTests -DskipITs -DskipCargo
RUN rm -rf soda4lca