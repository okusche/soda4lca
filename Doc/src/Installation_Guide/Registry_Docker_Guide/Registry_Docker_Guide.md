# Running the Registry application Docker image

This is the Registry application which allows to connect multiple soda4LCA nodes to a data network where a central authority controls which nodes and datasets are registered on the network.

A public Docker image for the Registry application is available at https://hub.docker.com/r/okworx/soda4lca-registry .

The following ENV variables must be set:

```
REGISTRY_NAME
REGISTRY_DESCRIPTION
REGISTRY_URL
MYSQL_HOST
MYSQL_PORT 
MYSQL_DATABASE
MYSQL_USER
MYSQL_PASSWORD
SODA_MAIL_HOST
SODA_MAIL_PORT 
SODA_MAIL_AUTH
SODA_MAIL_SENDER
SODA_MAIL_USER
SODA_MAIL_PASSWORD
```

Example:
```
REGISTRY_NAME="ACME Registry" 
REGISTRY_DESCRIPTION="Registry for ACME nodes" 
REGISTRY_URL=https://registry.acme.org 
MYSQL_HOST=host.docker.internal 
MYSQL_PORT=3306 
MYSQL_DATABASE=acme_registry 
MYSQL_USER=acmereg
MYSQL_PASSWORD=foobar
SODA_MAIL_HOST=smtp.acme.org
SODA_MAIL_PORT=587
SODA_MAIL_AUTH=true
SODA_MAIL_SENDER=noreply@registry.acme.org
SODA_MAIL_USER=reg_mail_user
SODA_MAIL_PASSWORD=barfoo
```
